Partie 2 : 
User Story


#3 En tant qu'utilisateur déjà inscrit je veux pouvoir me connecter afin de réserver des places de train pour un voyage et de faciliter mes réservations futures.

#2 En tant que client, je veux indiquer les informations ( lieu départ, arrivés, dates, trajet direct ou pas aller-simple/aller-retour afin d'avoir une liste de trains

#1 En tant que voyageur je veux choisir des places afin qu'elles soient proches les unes des autres.

Estimation de la date de terminaison du projet :
Total des points : 5 + 15 + 20 = 40
Durée estimée du projet  = (nombre de points / capacité initiale de l'équipe) * nombre de semaines par sprint.
(40/30)*1 = 1.3 mois

Partie 3 : spécifier les cas de tests fonctionnel


Matrice : https://docs.google.com/spreadsheets/d/1dryVZNp8Po-vAZVxUk9aqRotjRMKeVCGpfzRCMJEvZg/edit?usp=sharing






1
En tant que client je veux indiquer les informations (lieu de départ, arrivée, dates, trajet direct ou pas, aller simple, aller / retour) afin d’avoir une liste de train.

2
En tant que client je veux indiquer qui participe à un voyage afin d’obtenir des places cote à cote.

3
En tant que client je veux choisir la classe (1ere ou 2eme) afin de choisir  le niveau de confort.

4
En tant que client je veux me connecter afin de consulter mes réservations; avoir l’historique de mes réservations et réservez rapidement.

5
En tant que client je veux réserver mes billets de train afin de préparer mon voyage.

6
En tant qu’utilisateur je veux consulter des horaires afin d’avoir des infos précises (heures, numéro,train lieu).

7
En tant qu’utilisateur  je veux créer un compte afin d’avoir accès à mon espace client.

Critères de classement :
MVP (Minimal viable project) -> on commence par les fonctions essentiel
C’est un compromis entre le désir du client de l’architecture du logiciel
Ordre I -> satisfaire le client : =>MVP(1 . 2 . 5); 6 . 7 .4 . 3
Ordre II -> équipe de projet :
[6 => pour remplir la base de donnée; 1, 2, 5 ,7 4, 3] => ordre final (compromis) 6, 1, 2, 5, 7, 4, 3 .

Choix story de référence ( ou on voit ce qu’il faut faire) => fait par équipe de projet
7 -> 5 p
1 -> 15 p
2 -> 20
3 -> 14
4 -> 25
5 -> 15
6 -> 15

Total des points : 5+15+20+14+25+15+15 = 109

Durée estimée du projet  = (nombre de points / capacité initiale de l'équipe) * nombre de semaines par sprint.  
Définition capacité :  Nombre de points que l'équipe est capable de faire par sprint.
Définition sprint : Mini projet.

Durée estimée du projet : 
(109 / 30 points => c’est le choix de l’équipe) * 1 mois = 3,6 mois

Planification des stories

0	 1	   2	  3	     3,6 fin		4	
|6|1 | 2|5 | 7| 4   | 4   |
------------------------------------------
0    30     60     90                   points

![image-1.png](./image-1.png)
